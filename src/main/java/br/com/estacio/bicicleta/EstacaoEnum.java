package br.com.estacio.bicicleta;

import java.util.Optional;

public enum EstacaoEnum {

	// 1:springer, 2:summer, 3:fall, 4:winter
	PRIMAVERA("Primavera", 1), VERAO("VERÃO", 2), OUTONO("Outono", 3), INVERNO("Inverno", 4);

	private String nome;
	private int id;

	private EstacaoEnum(String nome, int id) {
		this.nome = nome;
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public int getId() {
		return id;
	}

	public static Optional<EstacaoEnum> getEstacaoPorId(int id) {
		Optional<EstacaoEnum> e = Optional.empty();
		for (EstacaoEnum estacao : EstacaoEnum.values()) {
			if(id == estacao.getId() ) {
				e = Optional.of(estacao);
				break;
			}
		}
		return e;
	}
	
	
}
