package br.com.estacio.bicicleta;

import java.util.Optional;

public enum PeriodoDiaEnum {
	
	PERIODO_1("1º Período", 0, 6),
	PERIODO_2("2º Período", 7, 12),
	PERIODO_3("3º Período", 13, 18),
	PERIODO_4("4º Período", 19, 23);
	
	private String descricao;
	private int limiteInferior;
	private int limiteSuperior;
	
	private PeriodoDiaEnum(String descricao, int limiteInferior, int limiteSuperior) {
		this.descricao = descricao;
		this.limiteInferior = limiteInferior;
		this.limiteSuperior = limiteSuperior;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getLimiteInferior() {
		return limiteInferior;
	}

	public int getLimiteSuperior() {
		return limiteSuperior;
	}
	
	public static Optional<PeriodoDiaEnum> getPeriodoPorHora(int hora) {
		Optional<PeriodoDiaEnum> p = Optional.empty();
		for(PeriodoDiaEnum periodo : PeriodoDiaEnum.values()) {
			if(hora >= periodo.getLimiteInferior() && hora <= periodo.getLimiteSuperior()) {
				p = Optional.of(periodo);
				break;
			}
		}
		return p;
	}
	
	
	
	
}
