package br.com.estacio.bicicleta;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class TotalBicicletasReducer extends Reducer<Text, LongWritable, Text, LongWritable> {
	
	@Override
	protected void reduce(Text key, Iterable<LongWritable> values, Context context) 
			throws IOException, InterruptedException {
	
		long total = 0L;
		
		for(LongWritable value : values ) {
			total += value.get();
		}
		
		context.write(key,  new LongWritable(total));
	}
}