package br.com.estacio.bicicleta;

import java.io.IOException;
import java.util.Optional;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TotalBicicletasMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
	private static final int POSICAO_ESTACAO = 2;	//1:springer, 2:summer, 3:fall, 4:winter
	private static final int POSICAO_ANO = 3;		//0: 2011, 1:2012
	private static final int POSICAO_HORA = 5;		//0 to 23
	private static final int POSICAO_TOTAL = 16;	
	private static final String ID_ANO_2011 = "0";	

	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
				
		String linha = value.toString();
		String[] dados = linha.split(",");
		if(dados.length > 0) {				
			int ano = ID_ANO_2011.equals(dados[POSICAO_ANO]) ? 2011 : 2012;

			int idEstacao = Integer.parseInt(dados[POSICAO_ESTACAO]);
			Optional<EstacaoEnum> estacao =  EstacaoEnum.getEstacaoPorId(idEstacao);

			int hora = Integer.parseInt(dados[POSICAO_HORA]);
			Optional<PeriodoDiaEnum> periodo = PeriodoDiaEnum.getPeriodoPorHora(hora);
			
			Long totalEstacaoAnoHora = Long.parseLong(dados[POSICAO_TOTAL]);
			
			if(estacao.isPresent() && periodo.isPresent()){
				
				StringBuilder chave = new StringBuilder();
				
				//Chave alfanumerica
				chave.append(ano);
				chave.append("_");
				chave.append(estacao.get().name());
				chave.append("_");
				chave.append(periodo.get().name());

				//Chave numerica
//				chave.append(ano);
//				chave.append(idEstacao);
//				chave.append(hora);

				context.write(new Text(chave.toString()), new LongWritable(totalEstacaoAnoHora.longValue()));
			}
		}
	}
}
